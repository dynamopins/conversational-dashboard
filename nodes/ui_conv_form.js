var request = require("request");
var mustache = require("mustache");
var util = require("util");
var vm = require("vm");

module.exports = function (RED) {
    var ui = require('../ui')(RED);

    function sendResults(node, _msgid, msgs) {
        if (msgs == null) {
            return;
        } else if (!util.isArray(msgs)) {
            msgs = [msgs];
        }
        var msgCount = 0;
        for (var m = 0; m < msgs.length; m++) {
            if (msgs[m]) {
                if (!util.isArray(msgs[m])) {
                    msgs[m] = [msgs[m]];
                }
                for (var n = 0; n < msgs[m].length; n++) {
                    var msg = msgs[m][n];
                    if (msg !== null && msg !== undefined) {
                        if (typeof msg === 'object' && !Buffer.isBuffer(msg) && !util.isArray(msg)) {
                            msg._msgid = _msgid;
                            msgCount++;
                        } else {
                            var type = typeof msg;
                            if (type === 'object') {
                                type = Buffer.isBuffer(msg) ? 'Buffer' : (util.isArray(msg) ? 'Array' : 'Date');
                            }
                            node.error(RED._("function.error.non-message-returned", {type: type}))
                        }
                    }
                }
            }
        }
        if (msgCount > 0) {
            node.send(msgs);
        }
    }

    function ConvFormNode(config) {
        RED.nodes.createNode(this, config);
        var node = this;

        var group = RED.nodes.getNode(config.group);
        if (!group) {
            return;
        }
        var tab = RED.nodes.getNode(group.config.tab);
        if (!tab) {
            return;
        }

        this.func = config.func;
        var nodeMethod = config.method || "GET";

        this.on("input", function (msg) {
            msg.topic = config.topic;
        });


        var done = ui.add({
            node: node,
            tab: tab,
            group: group,
            emitOnlyNewValues: false,
            control: {
                type: 'conv_form',
                label: config.label,
                order: config.order,
                value: config.payload || node.id,
                width: config.width || group.config.width || 6,
                height: config.height || config.options.length,
                options: config.options,
                formValue: config.formValue,
                submit: config.submit,
                cancel: config.cancel
            },
            beforeSend: function (msg) {
                /**
                 * Following code is copied from the function widget of Node RED:
                 * https://github.com/node-red/node-red/blob/master/nodes/core/core/80-function.js
                 */

                var functionText = "var results = null;" +
                    "results = (function(msg){ " +
                    "var __msgid__ = msg._msgid;" +
                    "var node = {" +
                    "id:__node__.id," +
                    "name:__node__.name," +
                    "log:__node__.log," +
                    "error:__node__.error," +
                    "warn:__node__.warn," +
                    "debug:__node__.debug," +
                    "trace:__node__.trace," +
                    "on:__node__.on," +
                    "status:__node__.status," +
                    "send:function(msgs){ __node__.send(__msgid__,msgs);}" +
                    "};\n" +
                    this.node.func + "\n" +
                    "})(msg);";

                var sandbox = {
                    console: console,
                    util: util,
                    Buffer: Buffer,
                    Date: Date,
                    RED: {
                        util: RED.util
                    },
                    __node__: {
                        id: node.id,
                        name: node.name,
                        log: function () {
                            node.log.apply(node, arguments);
                        },
                        error: function () {
                            node.error.apply(node, arguments);
                        },
                        warn: function () {
                            node.warn.apply(node, arguments);
                        },
                        debug: function () {
                            node.debug.apply(node, arguments);
                        },
                        trace: function () {
                            node.trace.apply(node, arguments);
                        },
                        send: function (id, msgs) {
                            sendResults(node, id, msgs);
                        },
                        on: function () {
                            if (arguments[0] === "input") {
                                throw new Error(RED._("function.error.inputListener"));
                            }
                            node.on.apply(node, arguments);
                        },
                        status: function () {
                            node.status.apply(node, arguments);
                        }
                    },
                    context: {
                        set: function () {
                            node.context().set.apply(node, arguments);
                        },
                        get: function () {
                            return node.context().get.apply(node, arguments);
                        },
                        keys: function () {
                            return node.context().keys.apply(node, arguments);
                        },
                        get global() {
                            return node.context().global;
                        },
                        get flow() {
                            return node.context().flow;
                        }
                    },
                    flow: {
                        set: function () {
                            node.context().flow.set.apply(node, arguments);
                        },
                        get: function () {
                            return node.context().flow.get.apply(node, arguments);
                        },
                        keys: function () {
                            return node.context().flow.keys.apply(node, arguments);
                        }
                    },
                    global: {
                        set: function () {
                            node.context().global.set.apply(node, arguments);
                        },
                        get: function () {
                            return node.context().global.get.apply(node, arguments);
                        },
                        keys: function () {
                            return node.context().global.keys.apply(node, arguments);
                        }
                    },
                    setTimeout: function () {
                        var func = arguments[0];
                        var timerId;
                        arguments[0] = function () {
                            sandbox.clearTimeout(timerId);
                            try {
                                func.apply(this, arguments);
                            } catch (err) {
                                node.error(err, {});
                            }
                        };
                        timerId = setTimeout.apply(this, arguments);
                        node.outstandingTimers.push(timerId);
                        return timerId;
                    },
                    clearTimeout: function (id) {
                        clearTimeout(id);
                        var index = node.outstandingTimers.indexOf(id);
                        if (index > -1) {
                            node.outstandingTimers.splice(index, 1);
                        }
                    },
                    setInterval: function () {
                        var func = arguments[0];
                        var timerId;
                        arguments[0] = function () {
                            try {
                                func.apply(this, arguments);
                            } catch (err) {
                                node.error(err, {});
                            }
                        };
                        timerId = setInterval.apply(this, arguments);
                        node.outstandingIntervals.push(timerId);
                        return timerId;
                    },
                    clearInterval: function (id) {
                        clearInterval(id);
                        var index = node.outstandingIntervals.indexOf(id);
                        if (index > -1) {
                            node.outstandingIntervals.splice(index, 1);
                        }
                    }
                };
                if (util.hasOwnProperty('promisify')) {
                    sandbox.setTimeout[util.promisify.custom] = function (after, value) {
                        return new Promise(function (resolve, reject) {
                            sandbox.setTimeout(function () {
                                resolve(value)
                            }, after);
                        });
                    }
                }

                var context = vm.createContext(sandbox);
                try {
                    this.node.script = vm.createScript(functionText, {
                        filename: 'Function node:' + this.node.id + (this.node.name ? ' [' + this.node.name + ']' : ''), // filename for stack traces
                        displayErrors: true
                        // Using the following options causes node 4/6 to not include the line number
                        // in the stack output. So don't use them.
                        // lineOffset: -11, // line number offset to be used for stack traces
                        // columnOffset: 0, // column number offset to be used for stack traces
                    });
                }
                catch (err) {
                    // eg SyntaxError - which v8 doesn't include line number information
                    // so we can't do better than this
                    //this.error(err);
                    this.node.error(err);
                }
                try {
                    var start = process.hrtime();
                    context.msg = msg;
                    this.node.script.runInContext(context);
                    sendResults(this.node, msg._msgid, context.results);
                    var duration = process.hrtime(start);
                    var converted = Math.floor((duration[0] * 1e9 + duration[1]) / 10000) / 100;
                    node.metric("duration", msg, converted);
                    if (process.env.NODE_RED_FUNCTION_TIME) {
                        this.node.status({fill: "yellow", shape: "dot", text: "" + converted});
                    }
                } catch (err) {
                    //remove unwanted part
                    var index = err.stack.search(/\n\s*at ContextifyScript.Script.runInContext/);
                    err.stack = err.stack.slice(0, index).split('\n').slice(0, -1).join('\n');
                    var stack = err.stack.split(/\r?\n/);

                    //store the error in msg to be used in flows
                    msg.error = err;

                    var line = 0;
                    var errorMessage;
                    if (stack.length > 0) {
                        while (line < stack.length && stack[line].indexOf("ReferenceError") !== 0) {
                            line++;
                        }

                        if (line < stack.length) {
                            errorMessage = stack[line];
                            var m = /:(\d+):(\d+)$/.exec(stack[line + 1]);
                            if (m) {
                                var lineno = Number(m[1]) - 1;
                                var cha = m[2];
                                errorMessage += " (line " + lineno + ", col " + cha + ")";
                            }
                        }
                    }
                    if (!errorMessage) {
                        errorMessage = err.toString();
                    }
                    node.error(errorMessage, msg);
                }
            },
            convert: function (payload) {
                const slotKeys = Object.keys(config.formValue);
                const payloadKeys = Object.keys(payload);
                slotKeys.forEach(slotKey => {
                    if (payloadKeys.includes(slotKey)) {
                        config.formValue[slotKey] = payload[slotKey];
                    }
                    else {
                        if (!payload["keepValues"]) {
                            config.formValue[slotKey] = "";
                        }
                    }
                });
                ui.updateUi();
            },
            convertBack: async function (payload) {

                //Fill the slot list to prepare a respond message and update the metadata
                let slotList = [];
                const slotKeys = Object.keys(config.options);
                slotKeys.forEach(slotKey => {
                    let option = config.options[slotKey];
                    let slotObject = {};
                    slotObject['slot_name'] = option.value;
                    slotObject['required'] = option.required;
                    slotObject['value'] = config.formValue[option.value];
                    slotObject['inquiry'] = option.inquiry;
                    slotObject['type'] = option.type;
                    slotList.push(slotObject);
                });

                //Check if all required slots are filled, otherwise return the inquiryText
                let keys = Object.keys(slotList);
                let response;
                keys.some(key => {
                    if (slotList[key].required) {
                        if (slotList[key].value === undefined || slotList[key].value === "") {
                            response = [{message: slotList[key].inquiry}, {json: null}, {statusCode: 0}];
                            return false;
                        }
                    }
                });
                if (response) {
                    return response;
                }

                //TODO Wird später nicht mehr verwendet
                payload = {slotList};


                try {
                    //If is templated url fill specific values into it
                    if ((config.url || "").indexOf("{{") != -1) {
                        config.templateUrl = mustache.render(config.url, config.formValue);
                    }
                }
                catch (error) {
                    return [{message: "Es ist ein Fehler beim Mapping der URL aufgetreten."},
                        {json: error}, {statusCode: 1}]
                }

                if (config.templateUrl != null && config.templateUrl !== "") {
                    //Execute the request with the configured method type and url
                    response = await new Promise(function (resolve, reject) {
                        request({method: config.method, uri: config.templateUrl},
                            function (error, response, body) {
                                //If the request was successful return the response with status code 2,
                                //otherwise throw an error
                                if (!error) {
                                    resolve([{message: null}, {json: body}, {statusCode: 2}]);
                                }
                                else {
                                    resolve([{message: "Es ist ein Fehler bei der Ausführung des Requests aufgetreten."},
                                        {json: error}, {statusCode: 1}]);
                                }
                            });
                    });
                }
                else {
                    return [{message: null}, {json: null}, {statusCode: 2}];
                }
                return response;
            }
        });
        node.on("close", done);
    }

    RED.nodes.registerType("ui_conv_form", ConvFormNode);
};
